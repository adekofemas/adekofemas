; DATA SHEET OF DIVIDER_SHELF_OP_DATA
; Index			DATA
; 0				Index of current division
; 1				Whether code pattern will be changed or not
; 2				Number of virtual divisions
; 3				Number of shelves in current division
; 4				Number of dividers in current division

; If module is notched, operations are not performed
(if (or (equal __NOTCHTYPE 0) (_NOTNULL GROUP_HOLES_FOR_NOTCH))
	(progn
		; DSOD prefix refers to DIVIDER_SHELF_OP_DATA
		(_FSET (_ 'DSOD_curDivOrder (getnth 0 DIVIDER_SHELF_OP_DATA)))
		(_FSET (_ 'DSOD_changeCodePattern (getnth 1 DIVIDER_SHELF_OP_DATA)))
		(_FSET (_ 'DSOD_numberOfVirtualDivisions (getnth 2 DIVIDER_SHELF_OP_DATA)))
		(_FSET (_ 'DSOD_numberOfShelves (getnth 3 DIVIDER_SHELF_OP_DATA)))
		(_FSET (_ 'DSOD_numberOfDividers (getnth 4 DIVIDER_SHELF_OP_DATA)))
		
		(_FSET (_ 'currentDivision (_& (_ "__DIV" DSOD_curDivOrder))))
		
		(_FSET (_ 'virtualDivisionCounter 0))
		(repeat DSOD_numberOfVirtualDivisions
			; Via DSOD_changeCodePattern parameter base of code bodies are determined
			(if (_NOTNULL DSOD_changeCodePattern)
				(progn
					; Sure that there are virtual divisions
					(_FSET (_ 'shelfCodeBody (_& (_ (XSTR "ADJUSTABLE_SHELF") "_" DSOD_curDivOrder "_" virtualDivisionCounter "_"))))
					(_FSET (_ 'paramBodyBase (_& (_ "ADJUSTABLE_SHELF_" DSOD_curDivOrder "_" virtualDivisionCounter "_"))))
					(_FSET (_ 'holeParamBodyBase (_& (_ "ADJUSTABLE_SHELF_HOLE_" DSOD_curDivOrder "_" virtualDivisionCounter "_"))))
				)
				(progn
					; No divider, no virtual division at all base of code bodies
					(_FSET (_ 'shelfCodeBody (_& (_ (XSTR "ADJUSTABLE_SHELF") "_" DSOD_curDivOrder "_"))))
					(_FSET (_ 'paramBodyBase (_& (_ "ADJUSTABLE_SHELF_" DSOD_curDivOrder "_"))))
					(_FSET (_ 'holeParamBodyBase (_& (_ "ADJUSTABLE_SHELF_HOLE_" DSOD_curDivOrder "_"))))
				)
			)
			; This variables prevents calling connection helper over and over in the same virtual division
			(_FSET (_ 'CHECK_CONNECTIONS nil))
			
			(_FSET (_ 'centerGroupIndex (+ 1 SHELF_HOLES_EXTRA_DOWN)))
			(_FSET (_ 'shelfCounter 1))
			(repeat DSOD_numberOfShelves
				; Unique code of adjustable shelves are determined here
				(_FSET (_ 'currentShelfCODE (_& (_ shelfCodeBody shelfCounter))))
				(if (_EXISTPANEL currentShelfCODE)
					(progn
						(_FSET (_ 'paramRoot (_& (_ paramBodyBase shelfCounter "_"))))
						
						(_FSET (_ 'shelfCONN (_S2V (_& (_ paramRoot "CONN")))))
						(_FSET (_ 'curDivELEV (_S2V (_& (_ currentDivision "_STARTFROMBOTTOM")))))
						; Global data container for connection helper is created here
						(_FSET (_ 'GROUP_HOLES_DATA (_ DSOD_curDivOrder virtualDivisionCounter DSOD_numberOfDividers curDivELEV shelfCONN)))
						
						; Connection helper is called for detecting current adjustable shelf will be connected which panels
						(if (null CHECK_CONNECTIONS) (_RUNDEFAULTHELPERRCP "CONNECTION_HELPER" nil "p2c"))
						
						; DATA SHEET OF CONN_HELPER_DATA
						; Index			DATA
						; 0				Data required about LEFT side
						; 1				Data required about RIGHT side
						
						(if (_NOTNULL CONN_HELPER_DATA)
							(progn
								; CHD prefix refers to CONN_HELPER_DATA
								(_FSET (_ 'CHD_leftSideInfo (getnth 0 CONN_HELPER_DATA)))
								(_FSET (_ 'CHD_rightSideInfo (getnth 1 CONN_HELPER_DATA)))
								
								; IMPORTANT INFORMATION
								; Each side info consists of -> Panel Code, Elevation, Panel Width, Tolerance Share, Hole Code For Rafix 
								
								(_FSET (_ 'leftSideCODE (getnth 0 CHD_leftSideInfo)))
								(_FSET (_ 'leftSideELEV (getnth 1 CHD_leftSideInfo)))
								(_FSET (_ 'leftSideWID (getnth 2 CHD_leftSideInfo)))
								(_FSET (_ 'leftSideTOLERANCE (getnth 3 CHD_leftSideInfo)))
								(_FSET (_ 'leftSideUPPEREXCESS (getnth 4 CHD_leftSideInfo)))
								(_FSET (_ 'leftSideLOWEREXCESS (getnth 5 CHD_leftSideInfo)))
								(_FSET (_ 'leftSideHEI (getnth 6 CHD_leftSideInfo)))
								
								(_FSET (_ 'rightSideCODE (getnth 0 CHD_rightSideInfo)))
								(_FSET (_ 'rightSideELEV (getnth 1 CHD_rightSideInfo)))
								(_FSET (_ 'rightSideWID (getnth 2 CHD_rightSideInfo)))
								(_FSET (_ 'rightSideTOLERANCE (getnth 3 CHD_rightSideInfo)))
								(_FSET (_ 'rightSideUPPEREXCESS (getnth 4 CHD_rightSideInfo)))
								(_FSET (_ 'rightSideLOWEREXCESS (getnth 5 CHD_rightSideInfo)))
								(_FSET (_ 'rightSideHEI (getnth 6 CHD_rightSideInfo)))
								
								(_FSET (_ 'currentShelfHEI (_S2V (_& (_ paramRoot "HEI")))))
								(_FSET (_ 'currentShelfWID (_S2V (_& (_ paramRoot "WID")))))
								(setq currentShelfPDATA (_S2V (strcat paramRoot "PDATA")))
								(setq currentShelfROT (_S2V (strcat paramRoot "ROT")))
								(setq currentShelfMAT (_S2V (strcat paramRoot "MAT")))
								(setq currentShelfTHICKNESS (_S2V (strcat paramRoot "THICKNESS")))
								
								(if (or (>= currentShelfHEI LIMIT_FOR_MIDDLE_HOLE_GROUP) (_NOTNULL IS_MIDDLE_HOLE_GROUP_AVAILABLE))
									(progn
										(_FSET (_ 'thirdHoleGroup T))
										(_FSET (_ 'thirdHoleGroupOffset (+ (/ currentShelfHEI 2.0) MIDDLE_HOLE_GROUP_OFFSET_FROM_CENTER_OF_SIDE)))
										(_FSET (_ 'holePosition_LM (+ leftSideTOLERANCE thirdHoleGroupOffset)))
										(_FSET (_ 'holePosition_RM (_= "rightSideWID - rightSideTOLERANCE - thirdHoleGroupOffset")))
										
										; Third hole group items are added
										(_ITEMMAIN SHELF_PIN_CODE currentShelfCODE (_ (* QUANTITY_OF_ITEM QUANTITY_OF_SHELF_PINS 1.5) SHELF_PIN_UNIT))
									)
									(progn
										(_FSET (_ 'thirdHoleGroup nil))
										(_ITEMMAIN SHELF_PIN_CODE currentShelfCODE (_ (* QUANTITY_OF_ITEM QUANTITY_OF_SHELF_PINS) SHELF_PIN_UNIT))
									)
								)
								
								(_FSET (_ 'activeShelfELEV (_S2V (_& (_ paramRoot "ELEV")))))
								(_FSET (_ 'leftSideOffset_Y (- (+ leftSideELEV activeShelfELEV) (/ ASHELF_HOLE_DIAMETER 2.0))))
								(_FSET (_ 'rightSideOffset_Y (- (+ rightSideELEV activeShelfELEV) (/ ASHELF_HOLE_DIAMETER 2.0))))
								
								(_FSET (_ 'holePosition_LF (_= "leftSideTOLERANCE + ASHELF_FRONT_HOLE_OFFSET + LEFT_PANEL_FRONT_VARIANCE")))
								(_FSET (_ 'holePosition_LB (_= "leftSideTOLERANCE + currentShelfHEI - ASHELF_BACK_HOLE_OFFSET + LEFT_PANEL_FRONT_VARIANCE")))
								(_FSET (_ 'holePosition_RF (_= "rightSideWID - rightSideTOLERANCE - ASHELF_FRONT_HOLE_OFFSET - RIGHT_PANEL_FRONT_VARIANCE")))
								(_FSET (_ 'holePosition_RB (_= "rightSideWID - rightSideTOLERANCE - currentShelfHEI + ASHELF_BACK_HOLE_OFFSET - RIGHT_PANEL_FRONT_VARIANCE")))
								
								(if IS_AVAILABLE_ASHELF_OVERALL_PIN_OP
									(progn
										(if (equal shelfCounter 1) 
											(progn
												(_FSET (_ 'maxOpSpaceLengthLeftSide (- leftSideHEI leftSideLOWEREXCESS leftSideUPPEREXCESS ASHELF_OVERALL_PIN_OP_LOWER_BEGINNING_ELEV ASHELF_OVERALL_PIN_OP_UPPER_ENDING_LIMIT)))
												(_FSET (_ 'maxOpSpaceLengthRightSide (- rightSideHEI rightSideLOWEREXCESS rightSideUPPEREXCESS ASHELF_OVERALL_PIN_OP_LOWER_BEGINNING_ELEV ASHELF_OVERALL_PIN_OP_UPPER_ENDING_LIMIT)))
												(_FSET (_ 'stepValueLeft (+ (fix (/ maxOpSpaceLengthLeftSide HOLE_GROUP_DISTANCE)) 1)))
												(_FSET (_ 'stepValueRight (+ (fix (/ maxOpSpaceLengthRightSide HOLE_GROUP_DISTANCE)) 1)))

												(_FSET (_ 'leftSideHoleStarting_Y (+ ASHELF_OVERALL_PIN_OP_LOWER_BEGINNING_ELEV leftSideLOWEREXCESS)))
												(_FSET (_ 'rightSideHoleStarting_Y (+ ASHELF_OVERALL_PIN_OP_LOWER_BEGINNING_ELEV rightSideLOWEREXCESS)))
												
												(_FSET (_ 'holePosition_L_Y leftSideHoleStarting_Y))
												(_FSET (_ 'index 0))
												(repeat stepValueLeft
													(_HOLE (_& (_ "ADJUSTABLE_SHELF_PIN_HOLE" index))  leftSideCODE (_ (_ holePosition_LF holePosition_L_Y 0.0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
													(_HOLE (_& (_ "ADJUSTABLE_SHELF_PIN_HOLE" index))  leftSideCODE (_ (_ holePosition_LB holePosition_L_Y 0.0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
													
													(if (_NOTNULL thirdHoleGroup)
														(progn
															(_HOLE (_& (_ "ADJUSTABLE_SHELF_PIN_HOLE" index))  leftSideCODE (_ (_ holePosition_LM holePosition_L_Y 0.0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
														)
													)
													(_FSET (_ 'holePosition_L_Y (+ holePosition_L_Y HOLE_GROUP_DISTANCE)))
													(_FSET (_ 'index (+ index 1)))
												)
												
												(_FSET (_ 'holePosition_R_Y rightSideHoleStarting_Y))
												(repeat stepValueRight
													(_HOLE (_& (_ "ADJUSTABLE_SHELF_PIN_HOLE" index))  rightSideCODE (_ (_ holePosition_RF holePosition_R_Y 0.0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
													(_HOLE (_& (_ "ADJUSTABLE_SHELF_PIN_HOLE" index))  rightSideCODE (_ (_ holePosition_RB holePosition_R_Y 0.0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
													
													(if (_NOTNULL thirdHoleGroup)
														(progn
															(_HOLE (_& (_ "ADJUSTABLE_SHELF_PIN_HOLE" index))  rightSideCODE (_ (_ holePosition_RM holePosition_R_Y 0.0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
														)
													)
													(_FSET (_ 'holePosition_R_Y (+ holePosition_R_Y HOLE_GROUP_DISTANCE)))
													(_FSET (_ 'index (+ index 1)))
												)
											)
										)
									)
									(progn
										; Center holes		
										(_FSET (_ 'holeCodeBody (_& (_ holeParamBodyBase shelfCounter "_"))))
										(_HOLE (_& (_ holeCodeBody centerGroupIndex "_LF")) leftSideCODE (_ (_ holePosition_LF leftSideOffset_Y 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
										(_HOLE (_& (_ holeCodeBody centerGroupIndex "_LB")) leftSideCODE (_ (_ holePosition_LB leftSideOffset_Y 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
										(_HOLE (_& (_ holeCodeBody centerGroupIndex "_RF")) rightSideCODE (_ (_ holePosition_RF rightSideOffset_Y 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
										(_HOLE (_& (_ holeCodeBody centerGroupIndex "_RB")) rightSideCODE (_ (_ holePosition_RB rightSideOffset_Y 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
										(if (_NOTNULL thirdHoleGroup)
											(progn
												(_HOLE (_& (_ holeCodeBody centerGroupIndex "_LM")) leftSideCODE (_ (_ holePosition_LM leftSideOffset_Y 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))		
												(_HOLE (_& (_ holeCodeBody centerGroupIndex "_RM")) rightSideCODE (_ (_ holePosition_RM rightSideOffset_Y 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
											)
										)
										; Upper-than-center holes
										(_FSET (_ 'stepCounter 1))
										(repeat SHELF_HOLES_EXTRA_UP
											(_FSET (_ 'currentStepHEI_LEFT (+ leftSideOffset_Y (* stepCounter HOLE_GROUP_DISTANCE))))
											(_FSET (_ 'currentStepHEI_RIGHT (+ rightSideOffset_Y (* stepCounter HOLE_GROUP_DISTANCE))))
											(_FSET (_ 'currentHoleIndex (+ centerGroupIndex stepCounter)))
											
											(_HOLE (_& (_ holeCodeBody currentHoleIndex "_LF")) leftSideCODE (_ (_ holePosition_LF currentStepHEI_LEFT 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
											(_HOLE (_& (_ holeCodeBody currentHoleIndex "_LB")) leftSideCODE (_ (_ holePosition_LB currentStepHEI_LEFT 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
											(_HOLE (_& (_ holeCodeBody currentHoleIndex "_RF")) rightSideCODE (_ (_ holePosition_RF currentStepHEI_RIGHT 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
											(_HOLE (_& (_ holeCodeBody currentHoleIndex "_RB")) rightSideCODE (_ (_ holePosition_RB currentStepHEI_RIGHT 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
											(if (_NOTNULL thirdHoleGroup)
												(progn
													(_HOLE (_& (_ holeCodeBody currentHoleIndex "_LM")) leftSideCODE (_ (_ holePosition_LM currentStepHEI_LEFT 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
													(_HOLE (_& (_ holeCodeBody currentHoleIndex "_RM")) rightSideCODE (_ (_ holePosition_RM currentStepHEI_RIGHT 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
												)
											)
											(_FSET (_ 'stepCounter (+ 1 stepCounter)))
										)
										; Lower-than-center holes
										(_FSET (_ 'stepCounter 1))
										(repeat SHELF_HOLES_EXTRA_DOWN
											(_FSET (_ 'currentStepHEI_LEFT (- leftSideOffset_Y (* stepCounter HOLE_GROUP_DISTANCE))))
											(_FSET (_ 'currentStepHEI_RIGHT (- rightSideOffset_Y (* stepCounter HOLE_GROUP_DISTANCE))))
											(_FSET (_ 'currentHoleIndex (- centerGroupIndex stepCounter)))
											
											(_HOLE (_& (_ holeCodeBody currentHoleIndex "_LF")) leftSideCODE (_ (_ holePosition_LF currentStepHEI_LEFT 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
											(_HOLE (_& (_ holeCodeBody currentHoleIndex "_LB")) leftSideCODE (_ (_ holePosition_LB currentStepHEI_LEFT 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
											(_HOLE (_& (_ holeCodeBody currentHoleIndex "_RF")) rightSideCODE (_ (_ holePosition_RF currentStepHEI_RIGHT 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
											(_HOLE (_& (_ holeCodeBody currentHoleIndex "_RB")) rightSideCODE (_ (_ holePosition_RB currentStepHEI_RIGHT 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
											(if (_NOTNULL thirdHoleGroup)
												(progn
													(_HOLE (_& (_ holeCodeBody currentHoleIndex "_LM")) leftSideCODE (_ (_ holePosition_LM currentStepHEI_LEFT 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
													(_HOLE (_& (_ holeCodeBody currentHoleIndex "_RM")) rightSideCODE (_ (_ holePosition_RM currentStepHEI_RIGHT 0) ASHELF_HOLE_DIAMETER ASHELF_HOLE_DEPTH))
												)
											)
											(_FSET (_ 'stepCounter (+ 1 stepCounter)))
										)
									)
								)
								(if IS_AVAILABLE_ASHELF_PIN_CONN_HOLE
									(progn
										(setq currentShelfCODE_sFace (_CREATESFACEMAIN currentShelfCODE (_ currentShelfPDATA currentShelfROT currentShelfMAT currentShelfTHICKNESS "Y")))
										
										(setq frontHoleOffset ASHELF_FRONT_HOLE_OFFSET)
										(setq backHoleOffset (- currentShelfHEI ASHELF_BACK_HOLE_OFFSET))
										(setq leftFrontHolePos (_ frontHoleOffset (- currentShelfWID ASHELF_PIN_CONN_HOLE_SIDE_OFFSET) 0))
										(setq leftBackHolePos (_ backHoleOffset (- currentShelfWID ASHELF_PIN_CONN_HOLE_SIDE_OFFSET) 0))
										(setq rightFrontHolePos (_ frontHoleOffset ASHELF_PIN_CONN_HOLE_SIDE_OFFSET 0))
										(setq rightBackHolePos (_ backHoleOffset ASHELF_PIN_CONN_HOLE_SIDE_OFFSET 0))
										(_HOLE "LF" currentShelfCODE_sFace (_ leftFrontHolePos ASHELF_PIN_CONN_HOLE_DIAMETER ASHELF_PIN_CONN_HOLE_DEPTH))
										(_HOLE "LB" currentShelfCODE_sFace (_ leftBackHolePos ASHELF_PIN_CONN_HOLE_DIAMETER ASHELF_PIN_CONN_HOLE_DEPTH))
										(_HOLE "RF" currentShelfCODE_sFace (_ rightFrontHolePos ASHELF_PIN_CONN_HOLE_DIAMETER ASHELF_PIN_CONN_HOLE_DEPTH))
										(_HOLE "RB" currentShelfCODE_sFace (_ rightBackHolePos ASHELF_PIN_CONN_HOLE_DIAMETER ASHELF_PIN_CONN_HOLE_DEPTH))
										(if (_NOTNULL thirdHoleGroup)
											(progn
												(setq thirdHoleOffset (+ (/ currentShelfHEI 2.0) MIDDLE_HOLE_GROUP_OFFSET_FROM_CENTER_OF_SIDE))
												(setq leftMiddleHolePos (_ thirdHoleOffset (- currentShelfWID ASHELF_PIN_CONN_HOLE_SIDE_OFFSET) 0))
												(setq rightMiddleHolePos (_ thirdHoleOffset ASHELF_PIN_CONN_HOLE_SIDE_OFFSET 0))
												
												(_HOLE "LM" currentShelfCODE_sFace (_ leftMiddleHolePos ASHELF_PIN_CONN_HOLE_DIAMETER ASHELF_PIN_CONN_HOLE_DEPTH))
												(_HOLE "RM" currentShelfCODE_sFace (_ rightMiddleHolePos ASHELF_PIN_CONN_HOLE_DIAMETER ASHELF_PIN_CONN_HOLE_DEPTH))
											)
										)
									)
								)
								(if IS_AVAILABLE_ASHELF_PIN_HORIZONTAL_CONN_HOLE
									(progn
										(_HOLE "LF" currentShelfCODE (_ (_ ASHELF_FRONT_HOLE_OFFSET currentShelfWID (* -1 ASHELF_PIN_HORIZONTAL_CONN_HOLE_HOR_POS)) ASHELF_PIN_HORIZONTAL_CONN_HOLE_DIAMETER ASHELF_PIN_HORIZONTAL_CONN_HOLE_DEPTH "Y-"))
										(_HOLE "LB" currentShelfCODE (_ (_ (- currentShelfHEI ASHELF_BACK_HOLE_OFFSET) currentShelfWID (* -1 ASHELF_PIN_HORIZONTAL_CONN_HOLE_HOR_POS)) ASHELF_PIN_HORIZONTAL_CONN_HOLE_DIAMETER ASHELF_PIN_HORIZONTAL_CONN_HOLE_DEPTH "Y-"))
										(_HOLE "RF" currentShelfCODE (_ (_ ASHELF_FRONT_HOLE_OFFSET 0.0 (* -1 ASHELF_PIN_HORIZONTAL_CONN_HOLE_HOR_POS)) ASHELF_PIN_HORIZONTAL_CONN_HOLE_DIAMETER ASHELF_PIN_HORIZONTAL_CONN_HOLE_DEPTH "Y+"))
										(_HOLE "RB" currentShelfCODE (_ (_ (- currentShelfHEI ASHELF_BACK_HOLE_OFFSET) 0.0 (* -1 ASHELF_PIN_HORIZONTAL_CONN_HOLE_HOR_POS)) ASHELF_PIN_HORIZONTAL_CONN_HOLE_DIAMETER ASHELF_PIN_HORIZONTAL_CONN_HOLE_DEPTH "Y+"))
										(if (_NOTNULL thirdHoleGroup)
											(progn
												(setq thirdHoleOffset (+ (/ currentShelfHEI 2.0) MIDDLE_HOLE_GROUP_OFFSET_FROM_CENTER_OF_SIDE))
												(setq leftMiddleHolePos (_ thirdHoleOffset currentShelfWID (* -1 ASHELF_PIN_HORIZONTAL_CONN_HOLE_HOR_POS)))
												(setq rightMiddleHolePos (_ thirdHoleOffset 0.0 (* -1 ASHELF_PIN_HORIZONTAL_CONN_HOLE_HOR_POS)))
												
												(_HOLE "LM" currentShelfCODE (_ leftMiddleHolePos ASHELF_PIN_HORIZONTAL_CONN_HOLE_DIAMETER ASHELF_PIN_HORIZONTAL_CONN_HOLE_DEPTH "Y-"))
												(_HOLE "RM" currentShelfCODE (_ rightMiddleHolePos ASHELF_PIN_HORIZONTAL_CONN_HOLE_DIAMETER ASHELF_PIN_HORIZONTAL_CONN_HOLE_DEPTH "Y+"))
											)
										)
									)
								)
							)
						)
					)
				)
				(_FSET (_ 'shelfCounter (+ 1 shelfCounter)))
			)
			; New virtual division
			(_FSET (_ 'virtualDivisionCounter (+ 1 virtualDivisionCounter)))
		)
		; Whether operation is called from notch recipe or not, following variable is set by nil
		(_FSET (_ 'GROUP_HOLES_FOR_NOTCH nil))
	)
)