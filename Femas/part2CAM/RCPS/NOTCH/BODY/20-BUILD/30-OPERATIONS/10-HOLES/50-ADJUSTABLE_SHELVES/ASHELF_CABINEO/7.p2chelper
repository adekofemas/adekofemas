; DATA SHEET OF DIVIDER_SHELF_OP_DATA
; Index			DATA
; 0				Index of current division
; 1				Whether code pattern will be changed or not
; 2				Number of virtual divisions
; 3				Number of shelves in current division
; 4				Number of dividers in current division

; DSOD prefix refers to DIVIDER_SHELF_OP_DATA
(_FSET (_ 'DSOD_curDivOrder (getnth 0 DIVIDER_SHELF_OP_DATA)))
(_FSET (_ 'DSOD_changeCodePattern (getnth 1 DIVIDER_SHELF_OP_DATA)))
(if (_NOTNULL DSOD_changeCodePattern)
	(progn
		(alert (strcat (XSTR "MODULE CODE") " : " __MODULCODE "\t" (strcat (XSTR "DIVISION") " : " (itoa DSOD_curDivOrder)) "\n\n" 
					   (XSTR "Notch operations aren't performed on divisions contains dividers!") "\n\n" "ASHELF_CABINEO -> " (itoa __NOTCHTYPE) ".p2chelper"))
	)
	(progn
		; Permission granted for operation
		(_FSET (_ 'DSOD_numberOfVirtualDivisions (getnth 2 DIVIDER_SHELF_OP_DATA)))
		(_FSET (_ 'DSOD_numberOfShelves (getnth 3 DIVIDER_SHELF_OP_DATA)))
		(_FSET (_ 'DSOD_numberOfDividers (getnth 4 DIVIDER_SHELF_OP_DATA)))
		
		(_FSET (_ 'currentDivision (_& (_ "__DIV" DSOD_curDivOrder))))
		
		(_FSET (_ 'virtualDivisionCounter 0))
		(repeat DSOD_numberOfVirtualDivisions
			(if (_NOTNULL DSOD_changeCodePattern)
				(progn
					; WARNING -> This branch of code is not performed in any condition
					; Sure that there are virtual divisions. That means notch operation wont effect the adjustable shelves in current division
					(_FSET (_ 'shelfCODEroot (XSTR "ADJUSTABLE_SHELF")))
					(_FSET (_ 'codeRoot "ADJUSTABLE_SHELF_"))
					(_FSET (_ 'virtualDivCode (_& (_ "_" virtualDivisionCounter "_"))))
					(_FSET (_ 'widDiffForNotch __NOTCHDIM2))
				)
				(progn
					; No divider no virtual division at all. That means notch operation will effect the adjustable shelves in current division
					(if (equal GROOVE_STATE 3)
						(progn
							(_FSET (_ 'shelfCODEroot (XSTR "NOTCHED_ADJUSTABLE_SHELF")))
							(_FSET (_ 'codeRoot "NOTCHED_ADJUSTABLE_SHELF_"))
						)
						(progn
							; GROOVE_STATE equals to either 1 or 2
							(_FSET (_ 'shelfCODEroot (XSTR "ADJUSTABLE_SHELF")))
							(_FSET (_ 'codeRoot "ADJUSTABLE_SHELF_"))
						)
					)
					(_FSET (_ 'virtualDivCode "_"))
					(_FSET (_ 'widDiffForNotch 0))
				)
			)
			; According to controls above code parameters are defined
			(_FSET (_ 'shelfCodeBody (_& (_ shelfCODEroot "_" DSOD_curDivOrder virtualDivCode))))
			(_FSET (_ 'paramBodyBase (_& (_ codeRoot DSOD_curDivOrder virtualDivCode))))
			
			; This variables prevents calling connection helper over and over in the same virtual division
			(_FSET (_ 'CHECK_CONNECTIONS nil))
			
			(_FSET (_ 'centerGroupIndex (+ 1 SHELF_HOLES_EXTRA_DOWN)))
			(_FSET (_ 'shelfCounter 1))
			(repeat DSOD_numberOfShelves
				(_FSET (_ 'currentShelfCODE (_& (_ shelfCodeBody shelfCounter))))
				(if (_EXISTPANEL currentShelfCODE)
					(progn
						(_FSET (_ 'paramRoot (_& (_ paramBodyBase shelfCounter "_"))))
						
						(_FSET (_ 'shelfCONN (_S2V (_& (_ paramRoot "CONN")))))
						(_FSET (_ 'curDivELEV (_S2V (_& (_ currentDivision "_STARTFROMBOTTOM")))))
						; Global data container for connection helper is created here
						(_FSET (_ 'GROUP_HOLES_DATA (_ DSOD_curDivOrder virtualDivisionCounter DSOD_numberOfDividers curDivELEV shelfCONN)))
						
						; Connection helper is called for detecting current adjustable shelf will be connected which panels
						(if (null CHECK_CONNECTIONS) (_RUNHELPERRCP (strcat "NOTCH\\NOTCH_HELPERS\\CONNECTION_HELPER\\" (itoa __NOTCHTYPE)) nil "p2chelper"))
						
						; DATA SHEET OF CONN_HELPER_DATA
						; Index			DATA
						; 0				Data required about LEFT side
						; 1				Data required about RIGHT side
						
						(if (_NOTNULL CONN_HELPER_DATA)
							(progn
								; CHD prefix refers to CONN_HELPER_DATA
								(_FSET (_ 'CHD_leftSideInfo (getnth 0 CONN_HELPER_DATA)))
								(_FSET (_ 'CHD_rightSideInfo (getnth 1 CONN_HELPER_DATA)))
								
								; IMPORTANT INFORMATION
								; Each side info consists of -> Panel Code, Elevation, Panel Width, Tolerance Share, Hole Code For Cabineo
								
								(_FSET (_ 'leftSideCODE (getnth 0 CHD_leftSideInfo)))
								(_FSET (_ 'leftSideELEV (getnth 1 CHD_leftSideInfo)))
								(_FSET (_ 'leftSideWID (getnth 2 CHD_leftSideInfo)))
								(_FSET (_ 'leftSideTOLERANCE (getnth 3 CHD_leftSideInfo)))
								(_FSET (_ 'holeCodeLEFT (getnth 3 CHD_leftSideInfo)))
								
								(_FSET (_ 'rightSideCODE (getnth 0 CHD_rightSideInfo)))
								(_FSET (_ 'rightSideELEV (getnth 1 CHD_rightSideInfo)))
								(_FSET (_ 'rightSideWID (getnth 2 CHD_rightSideInfo)))
								(_FSET (_ 'rightSideTOLERANCE (getnth 3 CHD_rightSideInfo)))
								(_FSET (_ 'holeCodeRIGHT (getnth 4 CHD_rightSideInfo)))
								
								(_FSET (_ 'currentShelfHEI (_S2V (_& (_ paramRoot "HEI")))))
								(_FSET (_ 'currentShelfWID (_S2V (_& (_ paramRoot "WID")))))
								(_FSET (_ 'currentShelfTHICKNESS (_S2V (_& (_ paramRoot "THICKNESS")))))
								(if (_NOTNULL ASHELF_HOLES_ON_LOWER_SURFACE)
									(progn
										(_FSET (_ 'currentShelfPDATA (_S2V (_& (_ paramRoot "PDATA")))))
										(_FSET (_ 'currentShelfROT (_S2V (_& (_ paramRoot "ROT")))))
										(_FSET (_ 'currentShelfMAT (_S2V (_& (_ paramRoot "MAT")))))
										
										(_FSET (_ 'currentShelfCODE (_CREATESFACEMAIN currentShelfCODE (_ currentShelfPDATA currentShelfROT currentShelfMAT currentShelfTHICKNESS "Y"))))
									)
								)
								(if (or (>= (- currentShelfHEI widDiffForNotch) LIMIT_FOR_MIDDLE_HOLE_GROUP) (_NOTNULL IS_MIDDLE_HOLE_GROUP_AVAILABLE))
									(progn
										(_FSET (_ 'thirdCabineo T))
										(_FSET (_ 'thirdCabineoOffset (+ (/ (- currentShelfHEI widDiffForNotch) 2.0) MIDDLE_HOLE_GROUP_OFFSET_FROM_CENTER_OF_SIDE)))
									)
									(_FSET (_ 'thirdCabineo nil))
								)
								(_FSET (_ 'activeShelfELEV (_S2V (_& (_ paramRoot "ELEV")))))
								(_FSET (_ 'halfOfThickness (/ currentShelfTHICKNESS 2.0)))
								(_FSET (_ 'leftSideOffset_Y (_= "leftSideELEV + activeShelfELEV + halfOfThickness + LEFT_PANEL_LOWER_VARIANCE")))
								(_FSET (_ 'rightSideOffset_Y (_= "rightSideELEV + activeShelfELEV + halfOfThickness + RIGHT_PANEL_LOWER_VARIANCE")))
								
								(_FSET (_ 'frontHoleOffset ASHELF_FRONT_HOLE_OFFSET))
								(_FSET (_ 'backHoleOffset (- currentShelfHEI ASHELF_BACK_HOLE_OFFSET)))
								
								(_FSET (_ 'leftFrontHolePos (_ frontHoleOffset currentShelfWID (* halfOfThickness -1))))
								(_FSET (_ 'leftBackHolePos (_ backHoleOffset currentShelfWID (* halfOfThickness -1))))
								(_FSET (_ 'rightFrontHolePos (_ frontHoleOffset 0 (* halfOfThickness -1))))
								(_FSET (_ 'rightBackHolePos (_ backHoleOffset 0 (* halfOfThickness -1))))
								(_FSET (_ 'holeCodeBody "ADJUSTABLE_SHELF_CABINEO_HOLE_"))
								(_CABINEOMAIN (_& (_ holeCodeBody "LF")) currentShelfCODE (_ leftFrontHolePos CABINEO_PARAMETERS) "Y-")
								(_CABINEOMAIN (_& (_ holeCodeBody "LB")) currentShelfCODE (_ leftBackHolePos CABINEO_PARAMETERS) "Y-")
								(_CABINEOMAIN (_& (_ holeCodeBody "RF")) currentShelfCODE (_ rightFrontHolePos CABINEO_PARAMETERS) "Y+")
								(_CABINEOMAIN (_& (_ holeCodeBody "RB")) currentShelfCODE (_ rightBackHolePos CABINEO_PARAMETERS) "Y+")
								
								(_ITEMMAIN CABINEO_CODE currentShelfCODE (_ (* QUANTITY_OF_ITEM 4) CABINEO_UNIT))
								(if (_NOTNULL thirdCabineo)
									(progn
										(_FSET (_ 'thirdHoleOffset thirdCabineoOffset))
										
										(_FSET (_ 'leftMiddleHolePos (_ thirdHoleOffset currentShelfWID (* halfOfThickness -1))))
										(_FSET (_ 'rightMiddleHolePos (_ thirdHoleOffset 0 (* halfOfThickness -1))))
										
										(_FSET (_ 'holeCodeBody "ADJUSTABLE_SHELF_CABINEO_HOLE_"))
										(_CABINEOMAIN (_& (_ holeCodeBody "LM")) currentShelfCODE (_ leftMiddleHolePos  CABINEO_PARAMETERS) "Y-")
										(_CABINEOMAIN (_& (_ holeCodeBody "RM")) currentShelfCODE (_ rightMiddleHolePos  CABINEO_PARAMETERS) "Y+")
										
										(_ITEMMAIN CABINEO_CODE currentShelfCODE (_ (* QUANTITY_OF_ITEM 2) CABINEO_UNIT))
									)
								)
								; Hole positions
								(_FSET (_ 'holePosition_LF (+ leftSideTOLERANCE ASHELF_FRONT_HOLE_OFFSET LEFT_PANEL_FRONT_VARIANCE)))
								(_FSET (_ 'holePosition_LB (_= "leftSideTOLERANCE + currentShelfHEI - ASHELF_BACK_HOLE_OFFSET + LEFT_PANEL_FRONT_VARIANCE")))
								(_FSET (_ 'holePosition_RF (_= "rightSideWID - rightSideTOLERANCE - ASHELF_FRONT_HOLE_OFFSET - RIGHT_PANEL_FRONT_VARIANCE")))
								(_FSET (_ 'holePosition_RB (_= "rightSideWID - rightSideTOLERANCE - currentShelfHEI + ASHELF_BACK_HOLE_OFFSET - RIGHT_PANEL_FRONT_VARIANCE")))
								; Center holes
								(_CABINEOMAIN (_& (_ holeCodeLEFT centerGroupIndex "_FRONT")) leftSideCODE (_ (_ holePosition_LF leftSideOffset_Y 0) CABINEO_PARAMETERS) nil)
								(_CABINEOMAIN (_& (_ holeCodeLEFT centerGroupIndex "_BACK")) leftSideCODE (_ (_ holePosition_LB leftSideOffset_Y 0) CABINEO_PARAMETERS) nil)
								(_CABINEOMAIN (_& (_ holeCodeRIGHT centerGroupIndex "_FRONT")) rightSideCODE (_ (_ holePosition_RF rightSideOffset_Y 0) CABINEO_PARAMETERS) nil)
								(_CABINEOMAIN (_& (_ holeCodeRIGHT centerGroupIndex "_BACK")) rightSideCODE (_ (_ holePosition_RB rightSideOffset_Y 0) CABINEO_PARAMETERS) nil)
								(if (_NOTNULL thirdCabineo)
									(progn
										(_FSET (_ 'holePosition_LM (+ leftSideTOLERANCE thirdCabineoOffset)))
										(_FSET (_ 'holePosition_RM (_= "rightSideWID - rightSideTOLERANCE - thirdCabineoOffset")))
										
										(_CABINEOMAIN (_& (_ holeCodeLEFT centerGroupIndex "_MIDDLE")) leftSideCODE (_ (_ holePosition_LM leftSideOffset_Y 0) CABINEO_PARAMETERS) nil)
										(_CABINEOMAIN (_& (_ holeCodeRIGHT centerGroupIndex "_MIDDLE")) rightSideCODE (_ (_ holePosition_RM rightSideOffset_Y 0) CABINEO_PARAMETERS) nil)
									)
								)
								; Upper-than-center holes
								(_FSET (_ 'stepCounter 1))
								(repeat SHELF_HOLES_EXTRA_UP
									(_FSET (_ 'currentStepHEI_LEFT (+ leftSideOffset_Y (* stepCounter HOLE_GROUP_DISTANCE))))
									(_FSET (_ 'currentStepHEI_RIGHT (+ rightSideOffset_Y (* stepCounter HOLE_GROUP_DISTANCE))))
									(_FSET (_ 'currentHoleIndex (+ centerGroupIndex stepCounter)))
									
									(_CABINEOMAIN (_& (_ holeCodeLEFT currentHoleIndex "_FRONT")) leftSideCODE (_ (_ holePosition_LF currentStepHEI_LEFT 0) CABINEO_PARAMETERS) nil)
									(_CABINEOMAIN (_& (_ holeCodeLEFT currentHoleIndex "_BACK")) leftSideCODE (_ (_ holePosition_LB currentStepHEI_LEFT 0) CABINEO_PARAMETERS) nil)
									(_CABINEOMAIN (_& (_ holeCodeRIGHT currentHoleIndex "_FRONT")) rightSideCODE (_ (_ holePosition_RF currentStepHEI_RIGHT 0) CABINEO_PARAMETERS) nil)
									(_CABINEOMAIN (_& (_ holeCodeRIGHT currentHoleIndex "_BACK")) rightSideCODE (_ (_ holePosition_RB currentStepHEI_RIGHT 0) CABINEO_PARAMETERS) nil)
									(if (_NOTNULL thirdCabineo)
										(progn
											(_CABINEOMAIN (_& (_ holeCodeLEFT currentHoleIndex "_MIDDLE")) leftSideCODE (_ (_ holePosition_LM currentStepHEI_LEFT 0) CABINEO_PARAMETERS) nil)
											(_CABINEOMAIN (_& (_ holeCodeRIGHT currentHoleIndex "_MIDDLE")) rightSideCODE (_ (_ holePosition_RM currentStepHEI_RIGHT 0) CABINEO_PARAMETERS) nil)
										)
									)
									(_FSET (_ 'stepCounter (+ 1 stepCounter)))
								)
								; Lower-than-center holes
								(_FSET (_ 'stepCounter 1))
								(repeat SHELF_HOLES_EXTRA_DOWN
									(_FSET (_ 'currentStepHEI_LEFT (- leftSideOffset_Y (* stepCounter HOLE_GROUP_DISTANCE))))
									(_FSET (_ 'currentStepHEI_RIGHT (- rightSideOffset_Y (* stepCounter HOLE_GROUP_DISTANCE))))
									(_FSET (_ 'currentHoleIndex (- centerGroupIndex stepCounter)))
									
									(_CABINEOMAIN (_& (_ holeCodeLEFT currentHoleIndex "_FRONT")) leftSideCODE (_ (_ holePosition_LF currentStepHEI_LEFT 0) CABINEO_PARAMETERS) nil)
									(_CABINEOMAIN (_& (_ holeCodeLEFT currentHoleIndex "_BACK")) leftSideCODE (_ (_ holePosition_LB currentStepHEI_LEFT 0) CABINEO_PARAMETERS) nil)
									(_CABINEOMAIN (_& (_ holeCodeRIGHT currentHoleIndex "_FRONT")) rightSideCODE (_ (_ holePosition_RF currentStepHEI_RIGHT 0) CABINEO_PARAMETERS) nil)
									(_CABINEOMAIN (_& (_ holeCodeRIGHT currentHoleIndex "_BACK")) rightSideCODE (_ (_ holePosition_RB currentStepHEI_RIGHT 0) CABINEO_PARAMETERS) nil)
									(if (_NOTNULL thirdCabineo)
										(progn
											(_CABINEOMAIN (_& (_ holeCodeLEFT currentHoleIndex "_MIDDLE")) leftSideCODE (_ (_ holePosition_LM currentStepHEI_LEFT 0) CABINEO_PARAMETERS) nil)
											(_CABINEOMAIN (_& (_ holeCodeRIGHT currentHoleIndex "_MIDDLE")) rightSideCODE (_ (_ holePosition_RM currentStepHEI_RIGHT 0) CABINEO_PARAMETERS) nil)
										)
									)
									(_FSET (_ 'stepCounter (+ 1 stepCounter)))
								)
							)
						)
					)
				)
				(_FSET (_ 'shelfCounter (+ 1 shelfCounter)))
			)
			; Next virtual division
			(_FSET (_ 'virtualDivisionCounter (+ 1 virtualDivisionCounter)))
		)
	)
)