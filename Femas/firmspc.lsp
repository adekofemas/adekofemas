(defun c:xz ( / outPutDialogResult)
	(setq outPutDialogResult 99)
	(setq lmm-projectInfo (list "1" "" (lmm_getDate 1) (lmm_atof 1) nil nil))
	(lmm_showDrawings)
)
(defun c:ds ( / )
	(setq lmm-projectInfo (list (getvar "DWGNAME") "" (lmm_getDate 1) (lmm_atof 1) nil nil))
	(lmm_showLimitedCutList)
)
(defun c:xxx ( / )
	(setq lmm-projectInfo (list (getvar "DWGNAME") "" (lmm_getDate 1) (lmm_atof 1) nil nil))
	(lmm_showRequirementList)
)

(defun c:Org () (lmm_openRcpOrganiser))